


const getDicByKey = function(key){
	let dic = uni.getStorageSync("dictionary")
	if(dic && dic.length> 0){
		let val = dic.filter(item => item.dicKey == key)
		if(val && val[0]){
			return val[0].dicValue
		}
	}
}

export default{
	getDicByKey
}