package com.kaxiu.exception.handle;

import com.kaxiu.exception.BizException;
import com.kaxiu.vo.ResultDataWrap;
import com.thoughtworks.xstream.core.BaseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;

/**
 * @author: LiYang
 * @create: 2019-08-16 00:43
 * @Description:
 **/
@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {AuthenticationException.class,
            AuthorizationException.class,
            UnauthenticatedException.class,
            UnauthorizedException.class
    })
    public ResultDataWrap exceptionHandle(Exception e) { // 处理方法参数的异常类型
        return new ResultDataWrap(e.getMessage(), HttpStatus.UNAUTHORIZED.value());
    }

    @ExceptionHandler(BizException.class)
    @ResponseBody
    public ResultDataWrap handle(Exception e) {
        return new ResultDataWrap(e.getCause().getMessage(), 500);
    }

    @ExceptionHandler(FileNotFoundException.class)
    @ResponseBody
    public ResultDataWrap handle(FileNotFoundException e) {
        return new ResultDataWrap("文件不存在", 500);
    }

}
