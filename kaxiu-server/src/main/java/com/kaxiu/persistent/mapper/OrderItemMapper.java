package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单项 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
