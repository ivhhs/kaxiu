package com.kaxiu.config.mqtt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: LiYang
 * @create: 2019-08-10 22:58
 * @Description:
 **/
@Data
@Component
@ConfigurationProperties(prefix = "spring.mqtt")
@EnableConfigurationProperties(MqttProperties.class)
public class MqttProperties {

    private String username;

    private String password;

    private String url;

    private String producerClientId;

    private String consumerClientId;

    private String defaultTopic;

    private String locationTopic;

}
