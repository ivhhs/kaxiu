package com.kaxiu.config;

/**
 * @author: LiYang
 * @create: 2019-08-19 00:48
 * @Description: 常量设置
 **/
public class CommonConstant {

    /**
     * 支付状态
     * 0:未支付，1:已支付,2:已关闭;3,支付失败
     */
    public static final class PayStatus{

        public static final Integer PAY_STATUS_UN_PAY = 0;

        public static final Integer PAY_STATUS_PAYED = 1;

        public static final Integer PAY_STATUS_CLOSE = 2;

        public static final Integer PAY_STATUS_FAILED = 3;

    }

    /**
     * 订单状态
     * 状态，1：发起订单，未支付，2：已支付，等待接单，3：已接单，正在赶往现场，4：维修完成，5：取消订单
     */
    public static final class OrderStatus{

        public static final Integer ORDER_STATUS_UN_PAY = 1;

        public static final Integer ORDER_STATUS_PAYED_WATTING = 2;

        public static final Integer ORDER_STATUS_RECEIVE = 3;

        public static final Integer ORDER_STATUS_OVER = 4;

        public static final Integer ORDER_STATUS_CALCLE = 5;


    }

    /**
     * 账户id
     */
    public static final class AccountConstant{

        /**
         * 管理员账户id
         */
        public static final Long ACCOUNT_ADMIN_ID = 1L;

        /**
         * 中间管理资金账户id
         */
        public static final Long ACCOUNT_MIDDLEMAN_ID = 2L;

        /**
         * 流向,流入：1，流出：2
         */
        public static final Integer ACCOUNT_DIRECTION_IN = 1;
        public static final Integer ACCOUNT_DIRECTION_OUT = 2;

        /**
         * 变更业务类型：支付：1，提现：2
         */
        public static final Integer ACCOUNT_BIZ_TYPE_PAY = 1;
        public static final Integer ACCOUNT_BIZ_TYPE_WITHDRAW = 2;

        /**
         * 历史账户状态
         */
        public static final Integer ACCOUNT_LOG_STATUS_CALCLE = 2;



    }

    /**
     * redis key
     */
    public static final class RedisKey{

        // user openid
        public static final String USERINFO = "kaxiu:user:%s";

        // user id + mobile
        public static final String MOBILE_CODE = "kaxiu:user:%s:mobile:%s:code";

        public static final String SERVER_LOCATION = "kaxiu:user:%s:location";
    }

    /**
     * redis key
     */
    public static final class DicKey{

        public static final String DIC_KEY_RAKE = "rake";

    }

}
