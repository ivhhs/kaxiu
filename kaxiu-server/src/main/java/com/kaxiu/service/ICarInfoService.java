package com.kaxiu.service;

import com.kaxiu.persistent.entity.CarInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 车辆信息
 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface ICarInfoService extends IService<CarInfo> {

}
