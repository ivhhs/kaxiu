package com.kaxiu.service.impl;

import com.kaxiu.persistent.entity.AccountWithdraw;
import com.kaxiu.persistent.mapper.AccountWithdrawMapper;
import com.kaxiu.service.IAccountWithdrawService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 提现 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class AccountWithdrawServiceImpl extends ServiceImpl<AccountWithdrawMapper, AccountWithdraw> implements IAccountWithdrawService {

}
