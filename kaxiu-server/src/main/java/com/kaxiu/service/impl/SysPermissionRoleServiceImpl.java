package com.kaxiu.service.impl;

import com.kaxiu.persistent.entity.SysPermissionRole;
import com.kaxiu.persistent.mapper.SysPermissionRoleMapper;
import com.kaxiu.service.ISysPermissionRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限中间表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class SysPermissionRoleServiceImpl extends ServiceImpl<SysPermissionRoleMapper, SysPermissionRole> implements ISysPermissionRoleService {

}
