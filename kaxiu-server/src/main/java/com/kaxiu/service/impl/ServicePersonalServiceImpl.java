package com.kaxiu.service.impl;

import com.kaxiu.persistent.entity.ServicePersonal;
import com.kaxiu.persistent.mapper.ServicePersonalMapper;
import com.kaxiu.service.IServicePersonalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 维修人员信息表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class ServicePersonalServiceImpl extends ServiceImpl<ServicePersonalMapper, ServicePersonal> implements IServicePersonalService {

}
