package com.kaxiu.service;

import com.kaxiu.persistent.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface ISysRoleService extends IService<SysRole> {

    List<SysRole> getByUserId(Long id);
}
