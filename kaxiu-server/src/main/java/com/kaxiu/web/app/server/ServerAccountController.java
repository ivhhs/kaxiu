package com.kaxiu.web.app.server;


import com.kaxiu.common.base.AbstractBaseController;
import com.kaxiu.config.redis.RedisService;
import com.kaxiu.service.IAccountService;
import com.kaxiu.vo.ResultDataWrap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 资金账户表 前端控制器
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@RestController
@RequestMapping("/kaxiu/server/account")
public class ServerAccountController extends AbstractBaseController {

    @Resource
    private IAccountService accountService;

    @RequestMapping("")
    public ResultDataWrap getAccount(){
        return buildResult(accountService.getByUserId());
    }

}

