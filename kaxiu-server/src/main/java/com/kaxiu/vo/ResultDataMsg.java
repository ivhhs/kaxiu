package com.kaxiu.vo;

/**
 * 全局错误码
 * <p>
 * <pre>
 *     如果是其他模块可以单独定义错误码类继承此类，如：PayErrorMsg
 * </pre>
 *
 * @author : Hui.Wang [huzi.wh@gmail.com]
 * @version : 1.0
 * @created on  : 2017/6/3  上午1:24
 */
public class ResultDataMsg {



    public static final ResultDataMsg DEFAULT_SUCCESS = new ResultDataMsg(200, "success");


    public static final ResultDataMsg SERVER_ERROR = new ResultDataMsg(1001, "服务人员状态错误，请联系管理员！");

    public static final ResultDataMsg DEFAULT_ERROR = new ResultDataMsg(500, "服务器错误！");


    private Integer status;

    private String msg;

    public ResultDataMsg(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public ResultDataMsg() {
    }

    public Integer getStatus() {
        return status;
    }

    public ResultDataMsg setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public ResultDataMsg setMsg(String msg) {
        this.msg = msg;
        return this;
    }
}
